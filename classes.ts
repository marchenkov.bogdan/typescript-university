import { Grade } from "./types.js";
import { University , Rector , Faculty , Student , Course , Professor , Homework} from "./interfaces.js";


// Classes
export class UniversityImpl implements University {
  name: string;
  rector: Rector;
  faculties: Faculty[];

  constructor(name: string, rector: Rector) {
    this.name = name;
    this.rector = rector;
    this.faculties = [];
  }

  enrollStudent(student: Student): void {
    student.enroll();
  }

  graduateStudent(student: Student): void {
    if (student.getGrades().length === 5) {
      console.log(`${student.name} has graduated from ${this.name}.`);
    } else {
      console.log(`${student.name} completed all the required courses.`);
    }
  }

  setSalaryForStaff(salary: number): void {
    this.faculties.forEach((faculty) => {
      faculty.professors.forEach((professor) => {
        professor.setSalary(salary);
      });
    });
    this.rector.setSalary(salary * 2);
  }
}

export class FacultyImpl implements Faculty {
  name: string;
  courses: Course[];
  professors: Professor[];

  constructor(name: string) {
    this.name = name;
    this.courses = [];
    this.professors = [];
  }

  addProfessor(professor: Professor): void {
    this.professors.push(professor);
  }
}

export class CourseImpl implements Course {
  name: string;
  professor: Professor;
  private _topics: string[];

  constructor(name: string, professor: Professor) {
    this.name = name;
    this.professor = professor;
    this._topics = [];
  }

  set topics(value: string[]) {
    this._topics = value;
  }

  get topics(): string[] {
    return this._topics;
  }
}

export class StudentImpl implements Student {
  id: number;
  name: string;
  age: number;
  faculty: Faculty;
  course: Course;
  grades: Grade[];

  constructor(id: number, name: string, age: number, faculty: Faculty, course: Course) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.faculty = faculty;
    this.course = course;
    this.grades = [];
  }

  enroll(): void {
    console.log(`${this.name} enrolled in ${this.faculty.name}.`);
  }

  submitHomework(homework: Homework): void {
    const professor = this.course.professor;
    professor.gradeHomework(this, Grade.Excellent);
  }

  getGrades(): Grade[] {
    return this.grades;
  }
}

export class ProfessorImpl implements Professor {
  id: number;
  name: string;
  age: number;
  faculty: Faculty;
  private salary: number;

  constructor(id: number, name: string, age: number, faculty: Faculty) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.faculty = faculty;
    this.salary = 5000;
  }

  teachCourse(course: Course): void {
    console.log(`${this.name} is teaching ${course.name}.`);
  }

  gradeHomework(student: Student, grade: Grade): void {
    student.grades.push(grade);
    console.log(`${this.name} graded ${student.name}'s homework. Grade: ${grade}`);
  }

  getSalary(): number {
    return this.salary;
  }

  setSalary(salary: number): void {
    this.salary = salary;
  }
}

export class RectorImpl implements Rector {
  name: string;
  age: number;
  private salary: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
    this.salary = 10000;
  }

  hireProfessor(professor: Professor): void {
    professor.faculty.addProfessor(professor);
    console.log(`${this.name} hired ${professor.name} as a professor.`);
  }

  fireProfessor(professor: Professor): void {
    const index = professor.faculty.professors.indexOf(professor);
    if (index !== -1) {
      professor.faculty.professors.splice(index, 1);
      console.log(`${this.name} fired ${professor.name} from the faculty.`);
    }
  }

  getSalary(): number {
    return this.salary;
  }

  setSalary(salary: number): void {
    this.salary = salary;
  }
}

