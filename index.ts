
import { RectorImpl , UniversityImpl , FacultyImpl , ProfessorImpl , CourseImpl , StudentImpl } from "./classes.js";

// Usage
const rector = new RectorImpl('Petro Petrovich', 45);

const university = new UniversityImpl('KNEU', rector);

const faculty = new FacultyImpl('Computer Science');

rector.hireProfessor(new ProfessorImpl(2, 'Taras Tarasovich', 40, faculty));

rector.hireProfessor(new ProfessorImpl(2, 'Igor Igorevich', 35, faculty));

const course = new CourseImpl('Node', new ProfessorImpl(1, 'Igor Igorevich', 35, faculty));

course.topics = ['Topic 1', 'Topic 2', 'Topic 3'];

console.log(`Course topics: ${course.topics}`);

const student = new StudentImpl(12345, 'Bogdan', 20, faculty, course);

university.faculties.push(faculty);
university.enrollStudent(student);

student.submitHomework({ courseId: 1, studentId: 12345, content: 'Express' });

university.graduateStudent(student);

university.setSalaryForStaff(5000);

console.log(`Professor get salary: ${course.professor.getSalary()}`);
console.log(`Rector get salary: ${university.rector.getSalary()}`);

