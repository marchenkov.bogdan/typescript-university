import { Person , Student , Course , Professor , Homework} from "./interfaces.js";

// Enums
export enum Grade {
    Excellent = 'A',
    Good = 'B',
    Satisfactory = 'C',
    Poor = 'D',
    Fail = 'F',
  }
  
  // Custom Types
  export type CourseTopic = {
    name: string;
    course: Course;
  };
  
  //Generic
  export type RecordBookWithGrades<T extends Student> = {
    name: string;
    student: T;
  };
  
  // Utility Types
  export type ReadonlyCourse = Readonly<Course>;
  
  // Type Guards
  export function isProfessor(person: Person): person is Professor {
    return 'specialty' in person;
  }
  
  // Type Assertions
  export function assertProfessor(person: Person): asserts person is Professor {
    if (!isProfessor(person)) {
      throw new Error('Expected person to be a Professor');
    }
  }