import { Grade } from "./types.js";

// Interfaces
export interface University {
    name: string;
    rector: Rector;
    faculties: Faculty[];
    enrollStudent(student: Student): void;
    graduateStudent(student: Student): void;
    setSalaryForStaff(salary: number): void;
}

export interface Faculty {
    name: string;
    courses: Course[];
    professors: Professor[];
    addProfessor(professor: Professor): void;
}

export interface Course {
    name: string;
    professor: Professor;
    topics: string[];
}

export interface Person {
    name: string;
    age: number;
}

export interface Student extends Person {
    id: number;
    faculty: Faculty;
    course: Course;
    enroll(): void;
    submitHomework(homework: Homework): void;
    getGrades(): Grade[];
    grades: Grade[];
}

export interface Professor extends Person {
    id: number;
    faculty: Faculty;
    teachCourse(course: Course): void;
    gradeHomework(student: Student, grade: Grade): void;
    getSalary(): number;
    setSalary(salary: number): void;
}

export interface Rector extends Person {
    hireProfessor(professor: Professor): void;
    fireProfessor(professor: Professor): void;
    getSalary(): number;
    setSalary(salary: number): void;
}

export interface Homework {
    courseId: number;
    studentId: number;
    content: string;
}