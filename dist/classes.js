import { Grade } from "./types.js";
// Classes
export class UniversityImpl {
    constructor(name, rector) {
        this.name = name;
        this.rector = rector;
        this.faculties = [];
    }
    enrollStudent(student) {
        student.enroll();
    }
    graduateStudent(student) {
        if (student.getGrades().length === 5) {
            console.log(`${student.name} has graduated from ${this.name}.`);
        }
        else {
            console.log(`${student.name} completed all the required courses.`);
        }
    }
    setSalaryForStaff(salary) {
        this.faculties.forEach((faculty) => {
            faculty.professors.forEach((professor) => {
                professor.setSalary(salary);
            });
        });
        this.rector.setSalary(salary * 2);
    }
}
export class FacultyImpl {
    constructor(name) {
        this.name = name;
        this.courses = [];
        this.professors = [];
    }
    addProfessor(professor) {
        this.professors.push(professor);
    }
}
export class CourseImpl {
    constructor(name, professor) {
        this.name = name;
        this.professor = professor;
        this._topics = [];
    }
    set topics(value) {
        this._topics = value;
    }
    get topics() {
        return this._topics;
    }
}
export class StudentImpl {
    constructor(id, name, age, faculty, course) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.faculty = faculty;
        this.course = course;
        this.grades = [];
    }
    enroll() {
        console.log(`${this.name} enrolled in ${this.faculty.name}.`);
    }
    submitHomework(homework) {
        const professor = this.course.professor;
        professor.gradeHomework(this, Grade.Excellent);
    }
    getGrades() {
        return this.grades;
    }
}
export class ProfessorImpl {
    constructor(id, name, age, faculty) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.faculty = faculty;
        this.salary = 5000;
    }
    teachCourse(course) {
        console.log(`${this.name} is teaching ${course.name}.`);
    }
    gradeHomework(student, grade) {
        student.grades.push(grade);
        console.log(`${this.name} graded ${student.name}'s homework. Grade: ${grade}`);
    }
    getSalary() {
        return this.salary;
    }
    setSalary(salary) {
        this.salary = salary;
    }
}
export class RectorImpl {
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.salary = 10000;
    }
    hireProfessor(professor) {
        professor.faculty.addProfessor(professor);
        console.log(`${this.name} hired ${professor.name} as a professor.`);
    }
    fireProfessor(professor) {
        const index = professor.faculty.professors.indexOf(professor);
        if (index !== -1) {
            professor.faculty.professors.splice(index, 1);
            console.log(`${this.name} fired ${professor.name} from the faculty.`);
        }
    }
    getSalary() {
        return this.salary;
    }
    setSalary(salary) {
        this.salary = salary;
    }
}
