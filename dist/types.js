// Enums
export var Grade;
(function (Grade) {
    Grade["Excellent"] = "A";
    Grade["Good"] = "B";
    Grade["Satisfactory"] = "C";
    Grade["Poor"] = "D";
    Grade["Fail"] = "F";
})(Grade || (Grade = {}));
// Type Guards
export function isProfessor(person) {
    return 'specialty' in person;
}
// Type Assertions
export function assertProfessor(person) {
    if (!isProfessor(person)) {
        throw new Error('Expected person to be a Professor');
    }
}
